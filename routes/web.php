<?php

	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/

	Route::get('/', function () {
		return view('welcome');
	});


	Route::get('/nasa', [
		'as' => 'getNasaApiList',
		'uses' => 'Nasa\NasaController@index',
	]);
	Route::get('/nasa/sublist', [
		'as' => 'sublist',
		'uses' => 'Nasa\NasaController@sublist',
	]);

	Route::post('/nasa/new', [
		'as' => 'postNasaApi',
		'uses' => 'Nasa\NasaController@createNasaApi',
	]);

	Route::get('/nasa/delete', [
		'as' => 'deleteAllItems',
		'uses' => 'Nasa\NasaController@deleteAllItems',
	]);
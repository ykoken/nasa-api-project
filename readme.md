# **Laravel Nasa Api ile Tarihler Arasındaki Verileri Kuyruklama Yöntemiyle Database'e İşleme**

>  kurulum dosyalarına buradan ulaşabilirsiniz.
https://gitlab.com/ykoken/nasa-api-project.git

 **kurulum için gerekli işlemler**


*  `composer update`
*  `php artisan key:generate`
> App\Providers  altındaki AppServiceProdiver dosyasındaki "boot" alanında migrate işlemi öncesinde güncellemeyi yapınız.
 
<pre>
       public function boot()
       {
   	    Schema::defaultStringLength(191);
       }`
</pre>

>Daha sonrasında Migrate işlemini gerçekleştiriyoruz.
*  `php artisan migrate`

Laravel Queue İşlemini Çalıştırmak İçin 
* `php artisan queue:work`
>Burada otomatik olarak görevlendirme yapılmadı, test işlemi olduğu için geçici olarak elle tetikleme kullanıldı.

>Api http request için Guzzle kullanıldı

> * `composer require guzzlehttp/guzzle:~6.0`

Env dosyasında,
> * `QUEUE_CONNECTION=database`  sync değilde database den çekeceğimiz için env dosyasındaki satırı düzenliyoruz..

Örnek env dosyasını kopyalayın ve .env dosyasında gerekli yapılandırma değişikliklerini yapın 

>* `cp .env.example .env`

**Api Bilgileri**
> * `https://api.nasa.gov/planetary/apod?api_key=EZ2seh4IZg5I02OlWcqO6b7QeAQlYj2qLh3uLRyI`

Laravel versiyon olarak 5.8 kullanıldı,
Database olarak Mysql Kullanıldı. 

Proje içerisinde, Services, İnterface ve Repositoryler, Job işlemleri kullanıldı.

Response işlemleri, validasyonlar ve mesajlar eklenmedi. 

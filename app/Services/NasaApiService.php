<?php

	namespace App\Services;

	use GuzzleHttp\Client;

	class NasaApiService
	{

		public static function getNasaApiData($date)
		{

			$client   = new Client();
			$response = $client->request('GET', 'https://api.nasa.gov/planetary/apod?date=' . $date . '&api_key=EZ2seh4IZg5I02OlWcqO6b7QeAQlYj2qLh3uLRyI');
			$data     =  json_decode($response->getBody(),true);
			return   $data['url'];
		}
	}
<?php namespace App\Repositories\Nasa;

use App\Models\Nasa\NasaApiDaysModel;
use App\Models\Nasa\NasaApiModel;
use Carbon\Carbon;

class  NasaDayRepository implements NasaDayRepositoryInterface
{
	public function createNasaApiDay($id)
	{
		$getNasaApi = NasaApiModel::find($id);
		$getNasaApiStartDate = Carbon::parse($getNasaApi->start_date);
		$getNasaApiDiffDay   = $getNasaApiStartDate->diffInDays($getNasaApi->end_date);

		for ($i=0;$i <= $getNasaApiDiffDay;$i++)
		{
			$data = [
				'nasa_api_id' => $getNasaApi->id,
				'day'         => $i,
			];
			NasaApiDaysModel::create($data);
		}

		return $data ;
		//return NasaApiDaysModel::create($data);
	}
}
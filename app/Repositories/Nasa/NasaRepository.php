<?php namespace App\Repositories\Nasa;

use App\Models\Nasa\NasaApiModel;
use Illuminate\Support\Facades\DB;

class  NasaRepository implements NasaRepositoryInterface
{
	public function createNasaApi($data)
	{
		return NasaApiModel::create($data);
	}
}
<?php

	namespace App\Repositories\Nasa;

	interface NasaDayRepositoryInterface
	{

		public function createNasaApiDay($data);

	}
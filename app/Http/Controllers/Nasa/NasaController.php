<?php

	namespace App\Http\Controllers\Nasa;

	use App\Jobs\NasaApiJob;
	use App\Models\Nasa\NasaApiDaysModel;
	use App\Repositories\Nasa\NasaDayRepository;
	use App\Repositories\Nasa\NasaRepository;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Log;

	class NasaController extends Controller
	{
		private $nasaApi;
		private $nasaApiDay;

		/**
		 * @var NasaRepository
		 */
		public function __construct(NasaRepository $nasaApi, NasaDayRepository $nasaApiDay)
		{
			$this->nasaApi    = $nasaApi;
			$this->nasaApiDay = $nasaApiDay;
		}


		public function index()
		{
			return view('nasa.list');
		}

		public function createNasaApi(Request $request)
		{
			//$this->validate($request, [
			//	'start_date', 'required',
			//	'end_date'  , 'required|after_or_equal:start_date'
			//]);
			//TODO::Zaman kazanmak için validasyonları geçiyorun...
			$data = [
				'start_date' => $request->input('start_date'),
				'end_date' => $request->input('end_date'),
			];

			try {
				$nasaApi     = $this->nasaApi->createNasaApi($data);
				$nasaApiDays = $this->nasaApiDay->createNasaApiDay($nasaApi->id);

				$this->dispatch(new NasaApiJob($nasaApi->id));

				return redirect()->to(route('getNasaApiList'))->with('status', 'OK');
			} catch (\Exception $exception) {
				Log::critical($exception);
				return response()->json(['error' => __('Kayıtlar eklenirken sorun oluştu.')], 500);
			}

		}

		public function deleteAllItems()
		{
			DB::statement("SET foreign_key_checks=0");
			DB::table('nasa_api_days')->truncate();
			DB::table('nasa_api')->truncate();
			return redirect()->to(route('getNasaApiList'))->with('status', 'OK');
		}

		public function sublist()
		{
			$data = NasaApiDaysModel::all();
			return view('nasa._sublist', compact('data'));
		}


	}

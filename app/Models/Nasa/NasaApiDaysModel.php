<?php

namespace App\Models\Nasa;

use Illuminate\Database\Eloquent\Model;

class NasaApiDaysModel extends Model
{
    protected $table = "nasa_api_days";
    protected $guarded =['id'];

	public function nasa_api()
	{
		return $this->belongsTo(NasaApiModel::class);
    }
}

<?php

namespace App\Models\Nasa;

use Illuminate\Database\Eloquent\Model;

class NasaApiModel extends Model
{
    protected $table = "nasa_api";
    protected $guarded = ['id'];

	public function nasa_api_days()
	{
		return $this->hasMany(NasaApiDaysModel::class);
    }
    
}

<?php

	namespace App\Jobs;

	use App\Models\Nasa\NasaApiDaysModel;
	use App\Models\Nasa\NasaApiModel;
	use App\Services\NasaApiService;
	use Carbon\Carbon;
	use Illuminate\Bus\Queueable;
	use Illuminate\Queue\SerializesModels;
	use Illuminate\Queue\InteractsWithQueue;
	use Illuminate\Contracts\Queue\ShouldQueue;
	use Illuminate\Foundation\Bus\Dispatchable;
	use Illuminate\Support\Facades\Log;

	class NasaApiJob implements ShouldQueue
	{
		use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
		private $nasaApi;
		private $nasaData;

		/**
		 * Create a new job instance.
		 *
		 * @return void
		 */
		public function __construct($nasaApi)
		{
			$this->nasaApi = $nasaApi;
			$this->nasaApiDetail();
		}

		/**
		 * Execute the job.
		 *
		 * @return void
		 */
		public function handle()
		{

			$getNasaApiDaysData = NasaApiDaysModel::where('nasa_api_id', $this->nasaData->id)->get();

			$i                  = 0;
			foreach ($getNasaApiDaysData as $nasaApiDay) {
				$data = [
					'id' => $nasaApiDay->id,
					'url' => NasaApiService::getNasaApiData(Carbon::createFromFormat('Y-m-d',$this->nasaData->start_date)->addDay($i)->format('Y-m-d')),
					'status' => 1
				];
				Log::info($data);
				NasaApiDaysModel::where('id',$nasaApiDay->id)->update($data);
				$i++;

			}
		}

		public function nasaApiDetail()
		{
			$this->nasaData = NasaApiModel::find($this->nasaApi);
		}

	}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNasaApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nasa_api', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->date('start_date');
            $table->date('end_date');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });

	    Schema::create('nasa_api_days', function (Blueprint $table) {
		    $table->integerIncrements('id');
		    $table->integer('nasa_api_id')->unsigned();
		    $table->integer('day');
		    $table->text('url')->nullable();
		    $table->tinyInteger('status')->default(0);
		    $table->timestamps();

	    });
	    Schema::table('nasa_api_days', function (Blueprint $table) {
		    $table->foreign('nasa_api_id')->references('id')->on('nasa_api')->onDelete('cascade')->onUpdate('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nasa_api');
        Schema::dropIfExists('nasa_api_days');
    }
}
